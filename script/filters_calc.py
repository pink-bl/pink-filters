import filters_config
import numpy as np
from xraydb import XrayDB

class TransmissionCalc:
    # Calculates filter transmission for PINK beamline
    def __init__(self):
        self.MaterialList = filters_config.MaterialList
        self.MaterialDensity = filters_config.MaterialDensity
        self.filter_thickness = [filters_config.MaterialThicknessesAttenuatorA, filters_config.MaterialThicknessesAttenuatorB, filters_config.MaterialThicknessesAttenuatorC]
        self.xraydb = XrayDB()

    def calc_filter_transmission(self, element, density, thickness, BeamEnergy):
        crossSection = self.xraydb.cross_section_elam(element, BeamEnergy, kind='photo')
        exponent = -1.0 * crossSection * density * (1/10000) * thickness
        return np.exp(exponent)
        
    def filter_transmission(self, filter_number, energy):
        txvec = [0]*12
        for pos in range(12):
            tx=1.0
            for layer in range(3):
                thickness = self.filter_thickness[filter_number][pos][layer]
                if thickness!=0:
                    tx = tx*self.calc_filter_transmission(self.MaterialList[layer], self.MaterialDensity[layer], thickness, energy) 
            txvec[pos] = tx
        return txvec