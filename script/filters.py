#!/usr/bin/python3

import epics
import filters_calc
import threading
import time

PF = filters_calc.TransmissionCalc()
threadLock = threading.Semaphore()

FilterA_tx_array = [0]*12
FilterB_tx_array = [0]*12
FilterC_tx_array = [0]*12

def onChange(pvname=None, value=None, char_value=None,**kw):
    threadLock.release()

## connecting PVs
print("Connecting to PVs...")
F1SelectA = epics.PV("PINK:FILTER:F1SelectA", auto_monitor=True)
F2SelectA = epics.PV("PINK:FILTER:F2SelectA", auto_monitor=True)
F3SelectA = epics.PV("PINK:FILTER:F3SelectA", auto_monitor=True)
F1SelectB = epics.PV("PINK:FILTER:F1SelectB", auto_monitor=True)
F2SelectB = epics.PV("PINK:FILTER:F2SelectB", auto_monitor=True)
F3SelectB = epics.PV("PINK:FILTER:F3SelectB", auto_monitor=True)
F1TxArray = epics.PV("PINK:FILTER:F1TxArray", auto_monitor=False)
F2TxArray = epics.PV("PINK:FILTER:F2TxArray", auto_monitor=False)
F3TxArray = epics.PV("PINK:FILTER:F3TxArray", auto_monitor=False)
BeamEnergy = epics.PV("PINK:FILTER:BeamEnergy", auto_monitor=True)
TxNow = epics.PV("PINK:FILTER:TxNow", auto_monitor=False)
TxSelected = epics.PV("PINK:FILTER:TxSelected", auto_monitor=False)

F1SelectA.add_callback(onChange)
F2SelectA.add_callback(onChange)
F3SelectA.add_callback(onChange)
F1SelectB.add_callback(onChange)
F2SelectB.add_callback(onChange)
F3SelectB.add_callback(onChange)
BeamEnergy.add_callback(onChange)

time.sleep(2)

print('[{}] Running filter trasmission calc script...'.format(time.asctime()))

while(1):
    threadLock.acquire()
    try:
        energy=BeamEnergy.value
        FilterA_tx_array = PF.filter_transmission(0,energy)
        FilterB_tx_array = PF.filter_transmission(1,energy)
        FilterC_tx_array = PF.filter_transmission(2,energy)
        F1TxArray.put(FilterA_tx_array)
        F2TxArray.put(FilterB_tx_array)
        F3TxArray.put(FilterC_tx_array)
        tx1 = FilterA_tx_array[int(F1SelectA.value)]
        tx2 = FilterB_tx_array[int(F2SelectA.value)]
        tx3 = FilterC_tx_array[int(F3SelectA.value)]
        TxNow.put(tx1*tx2*tx3)

        tx1 = FilterA_tx_array[int(F1SelectB.value)]
        tx2 = FilterB_tx_array[int(F2SelectB.value)]
        tx3 = FilterC_tx_array[int(F3SelectB.value)]
        TxSelected.put(tx1*tx2*tx3)
    except Exception as err:
        print("[{}] Main loop error:".format(time.asctime()))
        print(err)
        time.sleep(1)
