# -*- coding: utf-8 -*-
"""
Created on Tue Jan 30 13:48:27 2018

@author: julius

Altered on June 06 2019 by Nilson
"""
# Foil Materias are diamond, Aluminium and Tantalum
MaterialList = ['C', 'Al', 'Ta']

# Densities in [gm/cm^3], sources: diamond <- wikipedia, AL & Ta <- ELAM database
MaterialDensity = [3.52 , 2.694, 16.624]

# [0] = description, [1] = C_diamond [um], [2] = Al [um], [3] = Ta [um]
MaterialThicknessesAttenuatorA = [
    [  0,  0,   0],
    [  0,  0,   0],
    [ 20,  0,   0],
    [ 40,  0,   0],
    [ 20,  0,   0],
    [ 80,  0,   0],
    [ 60,  0,   0],
    [210,  0,   0],
    [150,  0,   0],
    [150, 50,   0],
    [  0,150,   0],
    [  0,  0,   0]]
# Filter A (first hit by beam)

MaterialThicknessesAttenuatorB = [
    [  0,  0,   0],
    [  0,  0,   0],
    [  0, 10,   0],
    [  0, 30,   0],
    [  0,105,   0],
    [  0,105,   5],
    [  0,105,  15],
    [  0,105,27.7],
    [  0,105,40.4],
    [  0,  0,   0],
    [  0,  0,   0],
    [  0,  0,   0]]

MaterialThicknessesAttenuatorC = [
    [  0,  0,   0],
    [  0,  0,   0],
    [  0, 25,   0],
    [  0, 75,   0],
    [  0, 75,   5],
    [  0, 75,  10],
    [  0, 75,22.7],
    [  0, 75,32.7],
    [  0,  0,   0],
    [  0,  0,   0],
    [  0,  0,   0],
    [  0,  0,   0]]


